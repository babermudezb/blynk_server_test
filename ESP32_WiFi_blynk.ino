#include <analogWrite.h>

/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************
  This example runs directly on ESP32 chip.

  Note: This requires ESP32 support package:
    https://github.com/espressif/arduino-esp32

  Please be sure to select the right ESP32 module
  in the Tools -> Board menu!

  Change WiFi ssid, pass, and Blynk auth token to run :)
  Feel free to apply it to any other example. It's simple!
 *************************************************************/

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial

#include "Arduino.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "ef4ed2b265744860b2e48f4eab0007f7";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Lilith8X";
char pass[] = ".35P-82GG#";

 


void setup()
{
  // Debug console
  Serial.begin(9600);
  pinMode(2, OUTPUT); 
  Blynk.begin(auth, ssid, pass, "blynk.thecloudcomputing.io", 8080);
}

void loop()
{
  Blynk.run();
}

BLYNK_WRITE(V1) //Button Widget is writing to pin V1
{
  int pinData = param.asInt(); 
  analogWrite(2,pinData);
  Blynk.virtualWrite(V2,pinData);
  Serial.println(pinData);
}

BLYNK_WRITE(V0) //Button Widget is writing to pin V0
{
  int pinData = param.asInt(); 

  if (pinData == 1)
  {
    analogWrite(2,255);
    Serial.println("LED Encendido");
  }else{ 
    analogWrite(2,0);
    Serial.println("LED Apagado");
  }
  
}
